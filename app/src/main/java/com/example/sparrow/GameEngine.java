package com.example.sparrow;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Rect;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;

public class GameEngine extends SurfaceView implements Runnable {
    private final String TAG = "SPARROW";

    // game thread variables
    private Thread gameThread = null;
    private volatile boolean gameIsRunning;

    // drawing variables
    private Canvas canvas;
    private Paint paintbrush;
    private SurfaceHolder holder;

    // Screen resolution varaibles
    private int screenWidth;
    private int screenHeight;

    // VISIBLE GAME PLAY AREA
    // These variables are set in the constructor
    int VISIBLE_LEFT;
    int VISIBLE_TOP;
    int VISIBLE_RIGHT;
    int VISIBLE_BOTTOM;

    int updatedY;
    int updatedX;

    int InitialY = 700;
    int InitialX = 100;

    // SPRITES
    Square bullet;
    int SQUARE_WIDTH = 50;

    Square enemy;

    Rect racketHitbox;

    Sprite player;
    Sprite sparrow;
    int sparrowX;
    int sparrowY;
    String catt = "left";
    Sprite cage;
    Sprite cat;

    ArrayList<Square> bullets = new ArrayList<Square>();

    Point restartButtonPosition;
    Point racketPosition;
    Point catPosition;
    final int DISTANCE_FROM_BOTTOM = 350;
    final int PADDLE_WIDTH = 200;
    final int PADDLE_HEIGHT = 100;

    // GAME STATS
    int score = 0;
    boolean gameOver = false;

    List<Sprite> bullettss = new ArrayList<Sprite>();

    public GameEngine(Context context, int screenW, int screenH) {
        super(context);

        // intialize the drawing variables
        this.holder = this.getHolder();
        this.paintbrush = new Paint();

        //this.racketHitbox = new Rect(racketPosition.x - 10,racketPosition.y - 10,racketPosition.x + 10,racketPosition.y + 10);

        this.bullet = new Square(context, 100, 250, SQUARE_WIDTH);

        // set screen height and width
        this.screenWidth = screenW;
        this.screenHeight = screenH;

        // setup visible game play area variables
        this.VISIBLE_LEFT = 20;
        this.VISIBLE_TOP = 10;
        this.VISIBLE_RIGHT = this.screenWidth - 20;
        this.VISIBLE_BOTTOM = (int) (this.screenHeight * 0.8);


        // initalize sprites


        this.player = new Sprite(this.getContext(), 100, 700, R.drawable.player64);
        this.sparrow = new Sprite(this.getContext(), 500, 200, R.drawable.bird64);
        this.cat = new Sprite(this.getContext(), 1500, 700, R.drawable.cat64);
        this.cage = new Sprite(this.getContext(), 1500, 100, R.drawable.player_laser);


        // racket
        racketPosition = new Point();
        racketPosition.x = VISIBLE_RIGHT - PADDLE_WIDTH;   // left
        racketPosition.y =  VISIBLE_TOP + PADDLE_HEIGHT;

        // restart button
        restartButtonPosition = new Point();
        restartButtonPosition.x = VISIBLE_LEFT + 100;   // left
        restartButtonPosition.y =  VISIBLE_BOTTOM -100 ;

    }

    @Override
    public void run() {
        while (gameIsRunning == true) {
            updateGame();    // updating positions of stuff
            updatePositions();
            redrawSprites(); // drawing the stuff

            controlFPS();
        }
    }

    boolean movingleft = true;
    boolean movingRight = true;
    final int PADDLE_SPEED = 20;
    final int CAT_SPEED = 15;
    //final int randomXPos = ;

    // Game Loop methods
    public void updateGame() {

        Random r = new Random();
        int randomXPos = r.nextInt(this.screenWidth) + 1;
        int randomYPos = r.nextInt(this.screenHeight) + 1;



        // @TODO: Update the position of the sprites


        // ---------------------------
        // Make racket move
        // ---------------------------
        if (movingRight == true) {
            //racketPosition.x = racketPosition.x + PADDLE_SPEED;
            this.cage.setxPosition(this.cage.getxPosition() + this.PADDLE_SPEED);


        }
        else {
            this.cage.setxPosition(this.cage.getxPosition() - this.PADDLE_SPEED);


        }

        // @TODO: Collision detection code
        if (this.cage.getxPosition() > this.VISIBLE_RIGHT) {
            Log.d(TAG, "Racket reached right of screen. Changing direction!");
            movingRight = false;
        }

        if (this.cage.getxPosition() < 0) {
            Log.d(TAG, "Racket reached left of screen. Changing direction!");
            movingRight = true;
        }

        this.cage.updateHitbox();

        Log.d(TAG, "Racket x-position: " + racketPosition.x);



        // ---------------------------
        // Make cat move
        // ---------------------------
        if (movingleft == true) {
            this.cat.setxPosition(this.cat.getxPosition() + this.CAT_SPEED);


        }
        else {
            this.cat.setxPosition(this.cat.getxPosition() - this.CAT_SPEED);


        }

        // @TODO: Collision detection code
        if (this.cat.getxPosition() > this.VISIBLE_RIGHT) {
            Log.d(TAG, "cat reached right of screen. Changing direction!");
            movingleft = false;
        }

        if (this.cat.getxPosition() < this.VISIBLE_LEFT) {
            Log.d(TAG, "cat reached left of screen. Changing direction!");
            movingleft = true;
        }

        //Log.d(TAG, "cat x-position: " + catPosition.x);



        //___________
        // BULLET
        //____________

        Log.d(TAG,"Bullet position: " + this.bullet.getxPosition() + ", " + this.bullet.getyPosition());
        //Log.d(TAG,"Enemy position: " + this.racketPosition.x + ", " + this.racketPosition.y);



        // Moving bullet
        // MAKE BULLET MOVE

        // 1. calculate distance between bullet and enemy
        double a = updatedX- this.bullet.getxPosition();
        double b = updatedY- this.bullet.getyPosition();

        // d = sqrt(a^2 + b^2)

        double d = Math.sqrt((a * a) + (b * b));

        Log.d(TAG, "Distance to enemy: " + d);

        // 2. calculate xn and yn constants
        // (amount of x to move, amount of y to move)
        double xn = (a / d);
        double yn = (b / d);

        // 3. calculate new (x,y) coordinates
        int newX = this.bullet.getxPosition() + (int) (xn * 15);
        int newY = this.bullet.getyPosition() + (int) (yn * 15);

        this.bullet.setxPosition(newX);
        this.bullet.setyPosition(newY);

        //Upate hitbox

        this.bullet.updateHitbox();


        if (bullet.getHitbox().intersect(cage.getHitbox())) {

            // UPDATE THE cage movement
            this.cage.setyPosition(this.cage.getyPosition() + this.VISIBLE_BOTTOM);
            this.cage.updateHitbox();

            if(this.cage.getyPosition() > this.screenHeight-650){


            }


        }
    }









    public void outputVisibleArea() {




        Log.d(TAG, "DEBUG: The visible area of the screen is:");
        Log.d(TAG, "DEBUG: Maximum w,h = " + this.screenWidth +  "," + this.screenHeight);
        Log.d(TAG, "DEBUG: Visible w,h =" + VISIBLE_RIGHT + "," + VISIBLE_BOTTOM);
        Log.d(TAG, "-------------------------------------");
    }



    public void updatePositions() {

        Random rand = new Random();



        this.sparrowX = rand.nextInt(this.screenWidth-100);
        this.sparrowY = rand.nextInt(this.screenHeight-100);
        Log.d(TAG, "SPARROW: x=" + this.sparrowX + "Y:" + this.sparrowY);


//        try {
//
//            TimeUnit.SECONDS.sleep(1);
//
//        } catch (InterruptedException e) {
//            System.err.format("IOException: %s%n", e);
//        }


    }




    public void redrawSprites() {
        if (holder.getSurface().isValid()) {

            // initialize the canvas
            canvas = holder.lockCanvas();
            // --------------------------------

            // set the game's background color
            canvas.drawColor(Color.argb(255, 255, 255, 255));

            // setup stroke style and width
            paintbrush.setStyle(Paint.Style.FILL);
            paintbrush.setStrokeWidth(8);

            // --------------------------------------------------------
            // draw boundaries of the visible space of app
            // --------------------------------------------------------
            paintbrush.setStyle(Paint.Style.STROKE);
            paintbrush.setColor(Color.argb(255, 0, 128, 0));

            canvas.drawRect(VISIBLE_LEFT, VISIBLE_TOP, VISIBLE_RIGHT, VISIBLE_BOTTOM, paintbrush);
            this.outputVisibleArea();

            // --------------------------------------------------------
            // draw player and sparrow
            // --------------------------------------------------------

            // 1. player
            canvas.drawBitmap(this.player.getImage(), this.player.getxPosition(), this.player.getyPosition(), paintbrush);

            // 2. sparrow

//            Random rand = new Random();
//
//
//
//            this.sparrowX = rand.nextInt(this.screenWidth-100);
//            this.sparrowY = rand.nextInt(this.screenHeight-100);
//            Log.d(TAG, "SPARROW: x=" + this.sparrowX + "Y:" + this.sparrowY);
//
//            canvas.drawBitmap(this.sparrow.getImage(), this.sparrowX, this.sparrowY, paintbrush);
            canvas.drawBitmap(this.sparrow.getImage(), this.sparrowX, this.sparrowY, paintbrush);
            updatePositions();


            // 3. cat
//            int catLeft = catPosition.x;
//            int catTop = catPosition.y;
//            int catRight = catPosition.x + 2*PADDLE_WIDTH;
//            int catottom = catPosition.y + PADDLE_HEIGHT;
//


            canvas.drawBitmap(this.cat.getImage(), this.cat.getxPosition(), this.cat.getyPosition(), paintbrush);

            //4. racket
            canvas.drawBitmap(this.cage.getImage(), this.cage.getxPosition(), this.cage.getyPosition(), paintbrush);


            // draw the paddle
//            paintbrush.setStyle(Paint.Style.FILL);
//            int paddleLeft = racketPosition.x;
//            int paddleTop = racketPosition.y;
//            int paddleRight = racketPosition.x + 2*PADDLE_WIDTH;
//            int paddleBottom = racketPosition.y + PADDLE_HEIGHT;
//            canvas.drawRect(paddleLeft, paddleTop, paddleRight, paddleBottom, paintbrush);


            // 1. restart button
            canvas.drawText("RESTART GAME", restartButtonPosition.x, restartButtonPosition.y, paintbrush);

            // --------------------------------------------------------
            // draw hitbox on player
            // --------------------------------------------------------
            Rect r = player.getHitbox();
            paintbrush.setStyle(Paint.Style.STROKE);
            canvas.drawRect(r, paintbrush);


            // --------------------------------------------------------
            // draw hitbox on player
            // --------------------------------------------------------
            paintbrush.setTextSize(60);
            paintbrush.setStrokeWidth(5);
            String screenInfo = "Screen size: (" + this.screenWidth + "," + this.screenHeight + ")";
            canvas.drawText(screenInfo, 10, 100, paintbrush);


            paintbrush.setColor(Color.BLACK);
            canvas.drawRect(
                    this.bullet.getxPosition(),
                    this.bullet.getyPosition(),
                    this.bullet.getxPosition() + this.bullet.getWidth(),
                    this.bullet.getyPosition() + this.bullet.getWidth(),
                    paintbrush
            );

            // --------------------------------------------------------
            // draw hitbox on racket
            // --------------------------------------------------------
            //Rect c = racket.getHitbox();
//            Rect cg = racketPosition.getHitbox();
//            paintbrush.setStyle(Paint.Style.STROKE);
//            canvas.drawRect(cg, paintbrush);


            //hit box on cage
            Rect cg = cage.getHitbox();
            paintbrush.setStyle(Paint.Style.STROKE);
            canvas.drawRect(cg, paintbrush);


            //hit box on bullet
            Rect bu = bullet.getHitbox();
            paintbrush.setStyle(Paint.Style.STROKE);
            canvas.drawRect(bu, paintbrush);


            // --------------------------------

            if (gameOver == true) {
                paintbrush.setTextSize(60);
                paintbrush.setColor(Color.RED);
                paintbrush.setStrokeWidth(5);
                canvas.drawText("GAME OVER!", 50, 200, paintbrush);
            }
            holder.unlockCanvasAndPost(canvas);
        }

    }

    public void controlFPS() {
        try {
            gameThread.sleep(17);
        }
        catch (InterruptedException e) {

        }
    }


    // Deal with user input
    @Override
    public boolean onTouchEvent(MotionEvent event) {

        int userAction = event.getActionMasked();
        //@TODO: What should happen when person touches the screen?
        if (userAction == MotionEvent.ACTION_DOWN) {
            // user pushed down on screen

            Log.d(TAG, "The person tapped: (" + event.getX() + "," + event.getY() + ")");
            this.updatedX = (int)event.getX();
            this.updatedY = (int)event.getY();



        }
        else if (userAction == MotionEvent.ACTION_UP) {
            // user lifted their finger
            // for pong, you don't need this, so no code is in here

            this.bullet.setxPosition(this.InitialX);
            this.bullet.setyPosition(this.InitialY);
        }
        return true;
    }

    // Game status - pause & resume
    public void pauseGame() {
        gameIsRunning = false;
        try {
            gameThread.join();
        }
        catch (InterruptedException e) {

        }
    }
    public void  resumeGame() {
        gameIsRunning = true;
        gameThread = new Thread(this);
        gameThread.start();
    }

}

